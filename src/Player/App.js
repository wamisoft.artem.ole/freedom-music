import React, { useEffect, useState } from 'react'
import { getDataSearch } from '../store/reducer'

import Header from './components/Header/Header'
import Main from './components/Main/Main'
import Player from './components/Player/Player'
import { connect } from 'react-redux'

const App = ({ getState }) => {
  const [state, setstate] = useState(null)

  useEffect(() => {
    getDataSearch().then((res) => {
      setstate(res)
    })
  }, [])

  if (state) getState(state)

  return (
    <div className="PlayerApp">
      <Header />
      <Main />
      <Player />
    </div>
  )
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    getState: (arg) => dispatch({ type: 'GET_STATE', obj: arg }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
