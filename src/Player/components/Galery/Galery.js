import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import './Galery.css'

import GaleryItem from './GaleryItem'

const Galery = ({ searchTrackList }) => {
  const [tracks, setTracks] = useState(null)

  useEffect(() => {
    setTracks(searchTrackList)
  })

  return (
    <ul className="Galery">
      {tracks ? (
        tracks.map((el) => {
          return (
            <li key={el.id} className="Galery__item">
              <GaleryItem {...el} />
            </li>
          )
        })
      ) : (
        <h2>Loading...</h2>
      )}
    </ul>
  )
}
const mapStateToProps = (state) => {
  return {
    searchTrackList: state.searchTrackList,
    nextList: state.nextList,
  }
}

export default connect(mapStateToProps, null)(Galery)
