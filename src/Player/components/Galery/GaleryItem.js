import React from 'react'
import { connect } from 'react-redux'
import { CHANGE_THIS_TRACK } from '../../../store/types'

const GaleryItem = ({
  id,
  artist: { name },
  title,
  album: { cover_medium },
  duration,
  changeTrak,
}) => (
  <div
    style={{
      background: `center / cover no-repeat url(${cover_medium}) `,
      width: '100%',
      height: '100%',
    }}
  >
    <div className="Galery__item-mask">
      <div className="Galery__item-text">
        <span className="Galery__item-artist">{name}</span>
        <span className="Galery__item-track">{title}</span>
      </div>
      <button
        onClick={() => changeTrak(id)}
        className="Galery__item-play"
      ></button>
      <div className="Galery__item-info">
        <ul>
          <li className="heards "></li>
          <li className="heards "></li>
          <li className="heards "></li>
          <li className="heards "></li>
          <li className="heards "></li>
        </ul>

        <span className="rating">
          {id < 10 ? `#0${duration}` : `#${duration}`}
        </span>
      </div>
    </div>
  </div>
)
const mapStateToProps = (state) => {
  return {
    playTrack: state.playTrack,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    changeTrak: (id) => dispatch({ type: CHANGE_THIS_TRACK, id: id }),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(GaleryItem)
