import React from 'react'

const TimeLine = ({ fullTime, progresTime, changeCurrentTime }) => {
  const { minutes, seconds } = fullTime
  const timeOneProc = (minutes * 60 + seconds) / 100
  const progres = (progresTime.minutes * 60 + progresTime.seconds) / timeOneProc
  let progresTimeOut = '--:--'
  let fullTimeOut = '--:--'
  if (!isNaN(progresTime.minutes) && !isNaN(progresTime.seconds)) {
    progresTimeOut =
      progresTime.minutes + ':' + (progresTime.seconds > 9 ? progresTime.seconds : '0' + progresTime.seconds)
  }
  if (!isNaN(minutes) && !isNaN(seconds)) {
    fullTimeOut = `${minutes}:${seconds > 9 ? seconds : seconds === 0 ? '--:--' : '0' + seconds}`
  }
  return (
    <div className='TimeLine'>
      <div className='TimeLine__time'>{progresTimeOut}</div>
      <div className='TimeLine__progres-bar' onClick={changeCurrentTime}>
        <div className='TimeLine__progres' style={{ width: progres + '%' }}></div>
      </div>
      <div className='TimeLine__lenght'>{fullTimeOut}</div>
    </div>
  )
}

export default TimeLine
