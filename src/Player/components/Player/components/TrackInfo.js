import React, { Fragment } from 'react'
import { connect } from 'react-redux'

const TrackInfo = ({ playTrack }) => {
  return (
    <Fragment>
      <div className="TrackInfo__image">
        <img src={playTrack ? playTrack.album.cover_small : null} alt="" />
      </div>
      <div className="TrackInfo__text">
        <div className="TrackInfo__track">
          {playTrack ? playTrack.title : null}
        </div>
        <div className="TrackInfo__artist">
          {playTrack ? playTrack.artist.name : null}
        </div>
      </div>
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state,
    playTrack: state.playTrack,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(TrackInfo)
