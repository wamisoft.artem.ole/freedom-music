import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { SEARCH } from '../../../store/types'
import { getDataSearch } from '../../../store/reducer'
import './SearchPanel.css'
import iconSearch from './search67.png'

const SearchPanel = ({ searchTrack }) => {
  const [val, setVal] = useState(null)
  const [searchItem, setSearchItem] = useState([])

  useEffect(() => {
    searchTrack(searchItem)
  }, [searchItem])

  return (
    <form className="SearchPanel">
      <input
        type="text"
        placeholder="Search"
        className="search"
        onChange={(e) => {
          setVal(e.target.value)
        }}
        onKeyDown={(e) => {
          setVal(e.target.value)

          if (e.keyCode === 13) {
            getDataSearch(val).then(({ data }) => {
              setSearchItem(data)
            })
            e.preventDefault()
          }
        }}
      />
      <button
        className="search__btn"
        onClick={(e) => {
          e.preventDefault()
          getDataSearch(val).then(({ data }) => {
            setSearchItem(data)
          })
          searchTrack(searchItem)
        }}
      >
        <img src={iconSearch} alt="icon-search" />
      </button>
    </form>
  )
}

const mapStateToProps = (state) => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    searchTrack: (searchItem) =>
      dispatch({ type: SEARCH, updateSearch: searchItem }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchPanel)
