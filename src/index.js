import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import logger from 'redux-logger'
import reducer from './store/reducer'

import About from './pages/About/About'
import PlayerApp from './Player/App'
import { ErrorPage } from './pages/ErrorPage/ErrorPage'

import Navbar from './pages/components/Navbar/Navbar'
import Footer from './pages/components/Footer/Footer'
import Grid from '@material-ui/core/Grid'

const App = () => {
  const store = createStore(reducer, compose(applyMiddleware(logger)))

  const pathname = window.location.pathname

  const [onPlayer, setOnPlayer] = useState(pathname === '/freedom-music/player' ? true : false)
  const handlerPlayerOn = () => {
    setOnPlayer(!onPlayer)
  }

  let style = {}
  if (window.innerWidth < 1200 && window.innerWidth > 767) {
    style = { width: '85%' }
  }

  let Nav = onPlayer ? null : (
    <Grid className='wrapper' style={style}>
      <Grid item lg={8} style={{ position: 'relative' }}>
        <Navbar style={{ marginTop: 75 }} type='main' />
      </Grid>
    </Grid>
  )

  return (
    <Provider store={store}>
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        {Nav}
        <Switch>
          <Route path='/player' component={PlayerApp} />
          <Route path='/about'>
            <About handlerPlayerOn={handlerPlayerOn} />
          </Route>
          <Route path='/'>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                minHeight: '80vh',
                overflowX: 'hidden',
                paddingTop: '100px',
              }}
            >
              <ErrorPage clicked={() => setOnPlayer(true)} />
            </div>
          </Route>
        </Switch>

        {!onPlayer ? <Footer /> : null}
      </BrowserRouter>
    </Provider>
  )
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
