import { randomIndex } from './reducer'

export const changeRandomType = (state) => {
  return { ...state, random: !state.random }
}

export const changeRepeatType = (state) => {
  return { ...state, repeat: !state.repeat }
}

export const changeThisTrack = (state, action) => {
  const track = state.trackList.filter((el) => el.id === action.id)[0]
  if (state.playTrack.id !== track.id) {
    return {
      ...state,
      playTrack: track,
    }
  }
  return { ...state }
}

export const play_track = (state, action) => {
  return {
    ...state,
    playTrack: state.trackList.filter((trak) => {
      if (trak.id === action.id && state.playTrack.id !== action.id) {
        return trak
      }
    }),
  }
}

export const playAudio = (state) => {
  return {
    ...state,
    pasue: state.playing,
    playing: !state.playing,
  }
}

export const changePlayeTrack = (state, action) => {
  let i = state.trackList.indexOf(state.playTrack)

  if (!state.random) {
    switch (action.increment) {
      case '-':
        --i
        if (i <= 0) i = state.trackList.length - 1
        break
      case '+':
        if (i === state.trackList.length - 1) i = 0
        ++i
        break
      default:
    }
  } else {
    i = randomIndex(state.trackList.length)
  }
  return {
    ...state,
    playTrack: state.trackList[i],
  }
}
