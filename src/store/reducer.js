import {
  PLAY_TRACK,
  CHANGE_PLAY_TRACK,
  PLAY,
  SEARCH,
  CHANGE_THIS_TRACK,
  TYPE_REPEAT,
  TYPE_RANDOM,
  VOLUME_OF,
  VOLUME_CHANGE,
} from './types'

import { volumeChange, volumeOf } from './volumeReducer'
import {
  changeRandomType,
  changeRepeatType,
  changeThisTrack,
  play_track,
  playAudio,
  changePlayeTrack,
} from './playigReducer'
import { searchHandle, getState } from './stateReducer'

const inititalState = {
  trackList: null,
  searchTrackList: null,
  playTrack: null,
  nextList: null,
  random: false,
  playing: true,
  repeat: false,
  muted: false,
  volume: 0.1,
}

const reducer = (state = inititalState, action) => {
  switch (action.type) {
    case VOLUME_CHANGE:
      return volumeChange(state, action)
    case VOLUME_OF:
      return volumeOf(state, action)

    case TYPE_RANDOM:
      return changeRandomType(state, action)

    case TYPE_REPEAT:
      return changeRepeatType(state, action)

    case CHANGE_THIS_TRACK:
      return changeThisTrack(state, action)

    case 'SEE_MORE':
      console.log('SEE_MORE', action)
      const updateTrackLists = [...state.trackList, ...action.nextTracks]
      const result = new Set(updateTrackLists)
      console.log(result)
      return {
        ...state,
        trackList: updateTrackLists,
        searchTrackList: updateTrackLists,
        nextList: action.next,
      }
    case SEARCH:
      return searchHandle(state, action)

    case PLAY:
      return playAudio(state, action)

    case PLAY_TRACK:
      return play_track(state, action)

    case CHANGE_PLAY_TRACK:
      return changePlayeTrack(state, action)

    case 'GET_STATE':
      return getState(state, action)
    default:
      return state
  }
}

export default reducer

export function randomIndex(max = inititalState.trackList.length) {
  const random = (Math.random() * 100).toFixed()
  if (random < max) {
    return random
  } else {
    return randomIndex(max)
  }
}

export function changeProgressBar(e) {
  const target = e.target
  const targetPosition = target.getBoundingClientRect()
  const OneProcent = target.offsetWidth / 100
  const containerClickPosition = Math.floor(
    (e.clientX - targetPosition.x) / OneProcent
  )

  return containerClickPosition
}
export const transformTime = (duration) => {
  const Time = Math.floor(duration)
  const second = Time % 60
  const minutes = (Time - second) / 60
  return { minutes, seconds: Math.floor(second) }
}

export async function getDataSearch(text = 'q', url = null) {
  let get = url || `https://deezerdevs-deezer.p.rapidapi.com/search?q=${text}`
  const res = await fetch(get, {
    method: 'GET',
    headers: {
      'x-rapidapi-host': 'deezerdevs-deezer.p.rapidapi.com',
      'x-rapidapi-key': '816487390cmshe1247c9229145cep1efa28jsn3649996183ac',
    },
  })
  const json = await res.json()
  return await json
}
