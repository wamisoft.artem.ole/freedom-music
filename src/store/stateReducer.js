export const searchHandle = (state, action) => {
  return {
    ...state,
    searchTrackList: action.updateSearch,
    trackList: action.updateSearch,
  }
}

export const getState = (state, action) => {
  return {
    ...state,
    trackList: action.obj.data,
    searchTrackList: action.obj.data,
    playTrack: action.obj.data[0],
    nextList: action.obj.next,
  }
}
