import { changeProgressBar } from './reducer'

export const volumeChange = (state, action) => {
  const x = changeProgressBar(action.event) / 100
  return { ...state, muted: false, volume: x }
}

export const volumeOf = (state, action) => {
  return { ...state, muted: !state.muted }
}
